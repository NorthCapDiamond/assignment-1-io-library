section .text
 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, 60                     ; 'exit' syscall number
    syscall
ret 

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    cld 
    xor al, al
    mov rcx, rdi
    .cycle:
    scasb
    jnz .cycle
    lea rax, [rdi-1]
    sub rax, rcx
ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi
    call string_length              ; use our previous function
    pop rdi
    mov rdx, rax                    ; string length in bytes
    mov rax, 1                      ; 'write' syscall number
    mov rsi, rdi                    ; string adress                   
    mov rdi, 1                      ; stdout descriptor                                                    ; string length in bytes
    syscall
ret

; Принимает код символа и выводит его в stdout
print_char:
    dec rsp                         ; reserve in stack
    mov byte [rsp], dil             ; put 'byte'
    mov rax, 1                      ; 'write' syscall number
    mov rsi, rsp                    ; string adress  
    mov rdi, 1                      ; stdout descriptor 
    mov rdx, 1                      ; size is 1 byte
    syscall
    inc rsp                         ; fix stack 
ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov dil, 0xA                    ; 0xA -> rsi
    call print_char                 ; it's fully similar task
ret

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov rax, rdi   
    mov r11, 10   
    mov rcx, rsp 
    .cycle:
    xor rdx, rdx           
    div r11
    dec rsp 
    add dl, '0'
    mov byte[rsp], dl
    or rax, rax
    jnz .cycle
    sub rcx, rsp 
    mov rdx, rcx 
    mov rax, 1
    mov rsi, rsp 
    mov rdi, 1
    syscall
    add rsp, rdx
ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    push rdi
    mov rax, rdi
    xor rdi, rdi 
    mov r11, 10
    mov rcx, rsp
    cmp rax, 0 
    jl .Neg_check
    .cycle:
    xor rdx, rdx           
    div r11
    dec rsp 
    add dl, '0'
    mov byte[rsp], dl
    or rax, rax
    jnz .cycle
    or rdi, rdi 
    jnz .put_minus
    .return:
    sub rcx, rsp 
    mov rdx, rcx 
    mov rax, 1
    mov rsi, rsp 
    mov rdi, 1
    syscall
    add rsp, rdx
    pop rdi
ret

.Neg_check: 
    mov rdi, r11
    neg rax
jmp .cycle

.put_minus:
    dec rsp 
    mov byte[rsp], "-"
jmp .return

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rax, rax
    xor r10, r10

    .loop:
        mov al, [rdi + r10]
        sub al, [rsi + r10]
        cmp al, 0
        jne .nope
        cmp byte [rdi + r10], 0
        je .yes_equals
        inc r10
        jmp .loop

    .nope:
        xor rax, rax
        ret

    .yes_equals:
        xor rax, rax
        inc rax
        ret


; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    dec rsp
    xor rdi, rdi 
    xor rax, rax
    mov rdx, 1
    mov rsi, rsp 
    syscall
    or ah, al
    mov al, byte [rsp]
    inc rsp
ret 

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    xor rax, rax 
    mov r8, rdi ; pointer 
    mov r9, rsi ; length 
    xor r10, r10; counter
    .spaceSkip:
        call read_char
        or ah, ah 
        jz .end 
        cmp al, 0x9
        je .spaceSkip
        cmp al, 0xA
        je .spaceSkip
        cmp al, 0x20
        je .spaceSkip
        cmp al, 0
        je .end 
        cmp al, 4
        je .end 
        

    inc r10 
    cmp r10, r9
    jae .overflow
    mov byte[r8+r10-1], al  

    .cycle:
    call read_char
    or ah, ah 
    jz .end 
    cmp al, 0x9
    je .end
    cmp al, 0xA
    je .end
    cmp al, 0x20
    je .end
    cmp al, 0 
    je .end 
    cmp al, 4
    je .end 

    inc r10 
    cmp r10, r9
    je .overflow
    mov byte[r8+r10-1], al 
    jmp .cycle

.end: 
    mov byte[r8+r10], 0
    mov rax, r8
    mov rdx, r10
    mov rdi, r8 
    mov rsi, r9 
ret 

.overflow:
    xor rax, rax
    mov rdi, r8 
    mov rsi, r9 
ret

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rcx, rcx
    .cycle1:
    cmp byte[rdi+rcx], "0"
    jae .cont_check
    or rcx, rcx 
    jnz .last
    xor rdx, rdx 
    ret 
    .continue:
    inc rcx
    jmp .cycle1

.cont_check:
    cmp byte[rcx+rdi], "9"
    jbe .continue
    or rcx, rcx 
    jnz .last 
    xor rdx, rdx
    ret 

.last: 
    xor r10,r10
    mov rsi, 1 
    push rcx 
    .cycle:
    xor rax, rax
    mov al, byte[rdi+rcx-1]
    sub al, "0"
    xor rdx, rdx   
    mul rsi
    add r10, rax
    lea rsi, [rsi*4+rsi]
    shl rsi, 1 
    dec rcx 
    jnz .cycle
    pop rdx 
    mov rax, r10
    ret

; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    cmp byte[rdi], "-"
    jz .lower
    call parse_uint
    ret

ret 
.lower:
    inc rdi 
    call parse_uint
    or rdx, rdx 
    jz .nea
    inc rdx
    neg rax 
ret
.nea:
    xor rax, rax
ret
; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    push rdi 
    call string_length
    pop rdi
    inc rax 
    cmp rax, rdx
    ja .more
    mov rcx, rax 
    cld
    mov r10, rsi 
    mov rsi, rdi
    mov rdi, r10
    rep movsb
    ret
.more:
    xor rax, rax 
ret
